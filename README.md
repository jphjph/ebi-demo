# HTTP logging to SQL demonstration

- Author: James Hannah
- Email: james@jph.im
- Telephone: +447837566421

This repository is a solution to the following task:

> Please set up an HTTP server of your choice (e.g. httpd, Nginx, lighttpd). We
> want to store the access logs of your HTTP server in a database. Ideally, a
> new record should be created each time there is a new line in the access log.
> Feel free to use any logging framework/module and database you want to
> implement your solution. We would like you to present your solution in a
> reproducible manner. Solutions with the mentality of ‘infrastructure as code’
> are the most desirable.

My solution and the descriptions on how to verify operation are described below.

## Description of solution

I have implemented this setup using three main components:

- Apache HTTPD (v2.4), running in an almost-standard configuration with minor
  changes to log output and paths
- PostgreSQL (v13), running in the default configuration, with a configured
  username and password (for testing) and data in a docker volume/kubernetes
  pvc.
- Fluentbit (v1.7), used as a log forwarder, reading logs from a shared volume
  with the webserver, and shipping them to PostgreSQL over a local connection.

These components have been deployed using either `docker-compose` or Kubernetes,
instructions for the setup of both are below.

The repository contains the following files:

- Configuration files in `./config`, for Fluentbit and httpd
- Kubernetes manifests for these services in `./resources`
- A Docker-compose file which should produce a similar setup to the Kubernetes
  manifests
- A Kustomization file to enable configuration templating in Kubernetes
- This README

## Running the repository with kubectl

This repository is intended primarily to be used as a set of Kubernetes
manifests, which can be built using `kustomize` or `kubectl` as follows
(**support for `docker-compose` is also provided - see below for details**):

```shell
# run from the repository directory containing kustomization.yaml
kubectl kustomize .
```

Alternatively, the resources can be deployed directly to an existing Kubernetes
cluster, in the `ebidemo` namespace, which may be created and setup as follows:

```shell
kubectl create ns ebidemo
# run from the repository directory containing kustomization.yaml
kubectl apply -k .
```

The Kubernetes manifests setup Kubernetes services named `http` and `logdb` in
the `ebidemo` namespace, listening on ports 80 and 5432 respectively.

Depending on how you have your Kubernetes port-forwarding configured, you may
need to also run the following command to forward ports 8080 and 55432 on your
local machine to the services:

```shell
# To forward to the webserver
kubectl port-forward -n ebidemo svc/http 8080:80

# To forward to the database
kubectl port-forward -n ebidemo svc/logdb 55432:5432

## (note these need to keep running for the service to be accessible)
```

## Setup with docker-compose

The same configuration/resources can also be deployed with `docker-compose` for
local testing. To start the services with `docker-compose`, the commands are
below:

```shell
# run from the directory containing docker-compose.yml
docker compose up
```

Docker compose will automatically create three containers (PostgreSQL database,
HTTPd service and fluentbit log forwarder). Some volumes will also be created
for database data and log sharing.

Ports should be automatically forwarded from your local machine to the webserver
and database, these should be port 8080 for HTTPD and 55432 for the PostgreSQL
database.

## Verifying operation

The repository should setup two TCP services - HTTP listening on port 80 and
PostgreSQL listening on port 5432 (these will be available as port 8080 and
55432 on your local machine if the instructions above are used verbatim).

Pointing a web browser at httpd should render the classic HTTPD "It Works!" page
(black sans-serif text on a white background). HTTPS is not enabled for this
demo to avoid the complication of configuring trusted certificates, although in
a production setup this could be achieved with a terminating SSL proxy (e.g
HAProxy).

Each container should have relevant log output:

- The logs of the httpd container (part of the webserver Pod in Kubernetes)
  should contain httpd error/startup logs. Access logs are not emitted to stdout
  from this pod.
- The logs of the fluentbit container should contain parsed, tagged access logs
  from the webservice. These should have key/value pairs for relevant
  information about each web request. These are output to stderr for
  debugging/validation.
- The logs of the PostgreSQL container should be standard Postgres log messages
  related to startup/timings.

You can connect to the PostgreSQL container with any standard client using the
credentials below:

- Hostname: `localhost`
- Port: `8080`
- Username: `http`
- Password: `http`

It should also be possible to connect on one line with command-line `psql` as
follows:

```shell
psql -d 'host=127.0.0.1 port=55432 user=http password=http'
```

You should be able to see one table named `logs`, with a row for each HTTP
access to the webserver. Each row will have three fields - a static tag,
`tail.0`, a `timestamp` column containing the time that the access was reported
by httpd, and a `jsonb` column named `data` containing key/value pairs about the
request.

The `data` column is JSONB and contains the following key/value pairs for each
log line: `host`, `query`, `method`, `status`, `process` (HTTPD PID), `referer`,
`request`, `filename`, `remoteIP`, `userAgent`. These are the standard HTTPD
`combined` format log fields, more can be added if required by making a simple
modification to the `httpd.conf` file. Since PostgreSQL supports comprehensive
query operators and indexing on the JSON column, this data can be queried as if
it were individual rows or matched against JSON queries.

## Future work/considerations for production

I considered several alternatives for the log-shipping-to-database daemon used
in this project, but settled on Fluentbit as it's a tool I've used before that
is lightweight and comes with built-in support for uploading logs to PostgreSQL
so no external tool is required.

I selected PostgreSQL as it's the database software I'm most familiar with.
Unfortunately fluentbit does not support inserting logs into MySQL. If that
were required, it would be a simple switch to fluentd (fluentbit's slightly more
heavyweight cousin) which supports a wider range of outputs, including other SQL
databases.

I selected Apache HTTPD as it's a very common httpd, although this setup could
be converted to use any other reasonable HTTP server with mimimal effort, as
long as the log output can be presented as JSON, which may in fact be even
easier to configure in some other HTTP servers.

Another alternative I considered was using a Layer-7 HTTP Proxy such as HAProxy,
positioned in front of the actual HTTPD which could be configured to ship logs
to the shipper, which could also terminate HTTPS and perform backend
monitoring/loadbalancing to multiple servers. I considered that this would have
been too heavyweight for this simple demonstration however. In a production
setup with more than one backend httpd however, this may be a less
resource-heavy solution as there would only be one instance of the log shipping
daemon.

For the transport between the httpd and log shipper, I settled on simply having
both containers share a writable volume, and having fluentbit tail the logfile
created by HTTPD. In a more production setup, this would not be sustainable as
the logfile would tend to grow infinitely over time - therefore some log
rotation/truncation could be configured, or a FIFO or UNIX socket used in-place
of the tailed file. HTTPD could also be configured to log directly to a
fluentbit process, or use it as a syslog forwarder.

Some hardening of the deployment would be required for actual production use,
not to mention actual configuration of the HTTP daemon and the setup of the log
rotation/forwarding as described above. Indexes will need to be configured on
the PostgreSQL database if the log volumes are going to grow to a significant
size. Currently the (local) connection between fluentd and PostgreSQL is in
plaintext (including the password), which is sufficient for this demonstration
but should be hardened for production (e.g switched to client SSL
authentication).
